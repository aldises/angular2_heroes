import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import {Hero} from '../classe/hero';
import { HeroService } from '../services/hero.service';
import { HerohttpService } from '../services/herohttp.service';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})


export class HeroDetailComponent implements OnInit {
 
  @Input() hero: Hero;

  constructor(
  private heroService: HeroService,
  private herohttpService : HerohttpService,
  private route: ActivatedRoute,
  private location: Location
) {}

  ngOnInit(): void {
    this.route.params
      .switchMap((params: Params) => this.heroService.getHero(+params['id']))
      .subscribe(hero => this.hero = hero);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
  this.herohttpService.update(this.hero)
    .then(() => this.goBack());
  }

 

}
